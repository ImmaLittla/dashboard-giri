#!/usr/bin/env python3 
#coding: utf-8

import sys, time, requests, json, ast
from threading import Thread

import serial
from PyQt5.QtWidgets import QApplication,QLCDNumber,QPushButton, QHBoxLayout, QVBoxLayout, QWidget, QMainWindow, QMessageBox, QLabel, QSizePolicy
from PyQt5.QtCore import Qt, QTimer, QTime, QRect
from PyQt5.QtGui import QFont, QPixmap

""" Номер платформы участника """
PLATFORM = 0

""" Настройка Serial """
SERIALPORT = '/dev/ttyS1'
BAUDRATE = 115200

""" DEBUG """
DEBUG = True

def log(s):
	if DEBUG:
		print (s)

""" Адаптивный размер текста """
class StretchedLabel(QLabel):
    def __init__(self, *args, **kwargs):
        QLabel.__init__(self, *args, **kwargs)
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Ignored)

    def resizeEvent(self, evt):
        font = self.font()
        font.setPixelSize(self.height() * 0.15)
        self.setFont(font)

class MainWindow (QWidget):
	def __init__(self):
		super().__init__()

		''' Кол-во подходов '''
		self.num_count = 0 

		''' Статус таймера '''
		self.flag_timer = 0 

		''' Запрос в БД тред '''
		log("[DB] Creating DB request Thread")
		Thread(target = self.db_run).start()
		
		self.label_FIO=StretchedLabel('404')

		self.timer=QTimer(self)
		self.curr_time= QTime(0,0)

		self.lcd_timer=QLCDNumber()
		self.lcd_timer.display(self.curr_time.toString('mm:ss'))

		self.lcd_count=QLCDNumber()

		self.initUI()
		self.show()

		""" UART тред """
		log("[UART] Creating UART Thread")
		Thread(target = self.uart_run).start()
		#log("[THREAD] Thread created: {0}".format(uart_thread.getName()))

	def initUI(self):
		self.setGeometry(100,100,600,400)
		self.setWindowTitle('timer-gui')

		self.lcd_count.display(self.num_count)
		hbox = QHBoxLayout()
		hbox.addWidget(self.label_FIO)
		hbox.addWidget(self.lcd_count)
		vbox = QVBoxLayout()
		vbox.addLayout(hbox)
		vbox.addWidget(self.lcd_timer)

		self.setLayout(vbox)

	def keyPressEvent(self, e):
		if e.key() == Qt.Key_1:
			self.num_count += 1
			self.lcd_count.display(self.num_count)
		if e.key() == Qt.Key_2:
			self.timer_reset()
		if e.key() == Qt.Key_3:
			self.timer_start()
		if e.key() == Qt.Key_4:
			self.flag_timer = 0

	def timer_reset(self):
		self.curr_time = QTime(0,0)
		self.num_count = 0
		self.flag_timer = 0
		self.lcd_timer.display(self.curr_time.toString('mm:ss'))
		self.lcd_count.display(self.num_count)
			
	def timer_start(self):
		if (self.flag_timer == 0):
			self.timer.singleShot(1000,self.timer_tick)
			self.flag_timer = 1

	def timer_tick(self):
		if (self.flag_timer == 1):
			self.timer.singleShot(1000,self.timer_tick)
			self.curr_time = self.curr_time.addSecs(1)
			self.lcd_timer.display(self.curr_time.toString('mm:ss'))
			#log ("[TIMER] {0}".format(self.curr_time.toString()))

	""" Чтение по uart (usb->uart) """
	def uart_run(self):
		self.ser=serial.Serial()
		self.ser.port= SERIALPORT
		self.ser.baudrate= BAUDRATE
		self.ser.timeout=0
		self.ser.parity=serial.PARITY_NONE
		self.ser.bytesize=serial.EIGHTBITS
		self.ser.stopbits=serial.STOPBITS_ONE
		try:
			self.ser.open()
			self.ser.flushInput()
			self.ser.flushOutput()
			b = b''
			while (1):
				b = self.uart_read()
				b = b.strip()
				log ("[UART] Received message: {0}".format(b))
				if (len(b) > 22):
					#print (b[28])
					# I807B85902000058Bffe258020200
					# I000000000000000000010002000300
					if (b[26] == '1'):
						self.num_count += 1
					if (b[26] == '2'):
						self.num_count -= 1
					if (b[26] == '3'):
						self.timer_start()
					if (b[26] == '4'):
						self.timer_reset()
					self.lcd_count.display(self.num_count)
			self.ser.close()
		except Exception as e:
			log ("[UART] ERROR: {0}".format(e))
		finally:
			self.ser.close()

	def uart_write(self):
		self.ser.write(b'P\x0A\x0D')

	def uart_read(self):
		out = b''
		time.sleep(DELAY)
		while self.ser.inWaiting() > 0:
			out += self.ser.read(1)
		return out.decode("utf-8")

	def db_run(self):
		try:
			sportsman_name = " "
			response = requests.get('http://127.0.0.1:8000/dashboard_get/', 
    			params={"platform":"{0}".format(PLATFORM), "competition":"1"})
			log ("[DB] Received status header: {0}".format(response.status_code))
			if (response.status_code == requests.codes.ok):
				log ("[DB] Received message: {0}".format(response.content.decode()))
				#name = response.json()
				#name = response.text
				name = ast.literal_eval(response.text)
				json_loads = json.loads(json.dumps(name, ensure_ascii=False))

				# парсим id каждого участника
				for x in json_loads:
					log (x)
					log (json_loads[x])

				sportsman_name = json_loads["1"]
				self.label_FIO.setText(sportsman_name)

			else:
				log ("[DB] Connection error ")
				log ("[DB] Closing Thread ")
				return
		except Exception as e:
			log ("[DB] ERROR: {0}".format(e))
		finally:
			pass


if __name__ == '__main__':
	app=QApplication(sys.argv)

	exe = MainWindow()
	sys.exit(app.exec_())		
